# nginx-jwt

Custom NGINX config forked from https://aur.archlinux.org/packages/nginx-custom/ with added jwt support.

Note: This build requires AUR dependency [libjwt](https://aur.archlinux.org/packages/libjwt) to be installed first
